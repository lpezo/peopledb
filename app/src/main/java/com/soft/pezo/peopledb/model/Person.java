package com.soft.pezo.peopledb.model;

import android.content.ContentValues;

public class Person {
    private String nombre;
    private String dni;
    private int edad;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Person() {
    }

    public Person(String nombre, String dni, int edad, String biografia, int idphoto, int id) {
        this.nombre = nombre;
        this.dni = dni;
        this.edad = edad;
        this.biografia = biografia;
        this.idphoto = idphoto;
        this.id = id;

    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setBiografia(String biografia) {
        this.biografia = biografia;
    }

    public void setIdphoto(int idphoto) {
        this.idphoto = idphoto;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDni() {
        return dni;
    }

    public int getEdad() {
        return edad;
    }

    public String getBiografia() {
        return biografia;
    }

    public int getIdphoto() {
        return idphoto;
    }

    public int getId() {
        return id;
    }

    private String biografia;
    private int idphoto;
    private int id;     //Identificador

    public ContentValues toContentValues(){
        ContentValues values = new ContentValues();
        values.put("nombre", this.getNombre());
        values.put("dni", this.getDni());
        values.put("edad", this.getEdad());
        values.put("biografia", this.getBiografia());
        values.put("idfoto", this.getIdphoto());
        return values;
    }
}
