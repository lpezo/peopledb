package com.soft.pezo.peopledb.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.soft.pezo.peopledb.R;
import com.soft.pezo.peopledb.model.Person;

import java.util.ArrayList;
import java.util.List;

public class CustomerDBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "Customers.db";
    private static final int DATABASE_VERSION = 1;
    public List<Person> items = new ArrayList<>();

    public CustomerDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS CLIENTE (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "nombre VARCHAR(120)," +
            "dni VARCHAR(8)," +
            "edad INTEGER," +
            "biografia VARCHAR(240)," +
            "idfoto INTEGER)");

        CargaInicial(db);
    }

    private void CargaInicial(SQLiteDatabase db) {
        items.add(new Person("Juan Perez", "20202020", 30, "Ingeniero de Sistemas", R.drawable.face01, 0));
        items.add(new Person("Luis Pezo", "07483210", 40, "Ingeniero de Sistemas EPE", R.drawable.face02, 0));
        items.add(new Person("Ignacio", "12345670", 40, "Ingeniero de Sistemas EPE", R.drawable.face03, 0));
        items.add(new Person("Miguel Romero", "3344334", 42, "Administrador", R.drawable.face03, 0));

        for(Person p: items){
            db.insert("CLIENTE", null, p.toContentValues());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public List<Person> getClientes(){
        List<Person> lista = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.query("CLIENTE", null, null, null, null, null, "nombre");

        while (c.moveToNext()){
            lista.add(new Person(
               c.getString(1),
               c.getString(2),
               c.getInt(3),
               c.getString(4),
               c.getInt(5),
               c.getInt(0)
            ));
        }
        c.close();
        return lista;
    }

    public void SavePerson(Person p){
        SQLiteDatabase db = getReadableDatabase();
        db.insert("CLIENTE", null, p.toContentValues());
    }
}
