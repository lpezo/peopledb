package com.soft.pezo.peopledb;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.soft.pezo.peopledb.adapter.PersonAdapter;
import com.soft.pezo.peopledb.database.CustomerDBHelper;
import com.soft.pezo.peopledb.model.Person;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView reciclador;
    private List<Person> items = new ArrayList();
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;
    private FloatingActionButton btnAgregar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FillPerson();

        btnAgregar = findViewById(R.id.btnAgregar);

        reciclador = (RecyclerView) findViewById(R.id.reciclador);
        reciclador.setHasFixedSize(true);

        lManager = new LinearLayoutManager(this);
        reciclador.setLayoutManager(lManager);

        adapter = new PersonAdapter(items);
        reciclador.setAdapter(adapter);

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AgregarPersona();
            }
        });
    }

    private void AgregarPersona() {
        Intent act = new Intent(this, NewActivity.class);
        startActivity(act);
    }

    private void FillPerson() {
        CustomerDBHelper db = new CustomerDBHelper(this);
        items = db.getClientes();
    }
}
