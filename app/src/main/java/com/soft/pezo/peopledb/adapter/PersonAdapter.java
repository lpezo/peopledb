package com.soft.pezo.peopledb.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.soft.pezo.peopledb.R;
import com.soft.pezo.peopledb.model.Person;

import java.util.List;

/**
 * Created by Marlon on 15/08/2018.
 */

public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.PersonViewHolder>  {
    private List<Person> items;

    public static class PersonViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public CardView personCardView;
        public ImageView imgFoto;
        public TextView lblNombre;
        public TextView lblDNI;
        public TextView lblEdad;

        public PersonViewHolder(View v) {
            super(v);
            personCardView = (CardView) v.findViewById(R.id.personcardview);
            imgFoto = (ImageView) v.findViewById(R.id.imagen);
            lblNombre = (TextView) v.findViewById(R.id.nombre);
            lblDNI = (TextView) v.findViewById(R.id.dni);
            lblEdad = (TextView) v.findViewById(R.id.edad);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public PersonAdapter(List<Person> items) {
        this.items = items;
    }

    public List<Person> getItems(){
        return this.items;
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.person_card, viewGroup, false);
        return new PersonViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PersonViewHolder viewHolder, final int i) {
        viewHolder.imgFoto.setImageResource(items.get(i).getIdphoto());
        viewHolder.lblNombre.setText(items.get(i).getNombre());
        viewHolder.lblDNI.setText("DNI: " + items.get(i).getDni());
        viewHolder.lblEdad.setText("Edad: " + String.valueOf(items.get(i).getEdad()));

    }
}
