package com.soft.pezo.peopledb;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.soft.pezo.peopledb.database.CustomerDBHelper;
import com.soft.pezo.peopledb.model.Person;

import java.util.ArrayList;
import java.util.List;

public class NewActivity extends AppCompatActivity {

    private EditText txtNombre;
    private EditText txtDNI;
    private EditText txtEdad;
    private EditText txtBiografia;
    private Spinner cmbAvatar;
    private Button btnGuardar;

    private List<String> arrAvatarTexto = new ArrayList<>();
    private List<Integer> arrAvatarResource = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);

        txtNombre = findViewById(R.id.editNombre);
        txtDNI = findViewById(R.id.editDni);
        txtNombre = findViewById(R.id.editEdad);
        txtBiografia = findViewById(R.id.editBiografia);
        cmbAvatar = findViewById(R.id.editAutor);

        btnGuardar = findViewById(R.id.btnGuardar);
    }

    private void GuardarPersona() {
        if(txtNombre.getText().toString().isEmpty()){
            Toast.makeText(this, "Ingrese su nombre", Toast.LENGTH_SHORT).show();
            return;
        }
        if(txtDNI.getText().toString().isEmpty()){
            Toast.makeText(this, "Ingrese su DNI", Toast.LENGTH_SHORT).show();
            return;
        }
        if(txtEdad.getText().toString().isEmpty()){
            Toast.makeText(this, "Ingrese su edad", Toast.LENGTH_SHORT).show();
            return;
        }
        if(txtBiografia.getText().toString().isEmpty()){
            Toast.makeText(this, "Ingrese su biografia", Toast.LENGTH_SHORT).show();
            return;
        }

        String vNombre = txtNombre.getText().toString();
        String vDNI = txtDNI.getText().toString();
        int vEdad = Integer.parseInt(txtEdad.getText().toString());
        String vBiografia = txtBiografia.getText().toString();

        int selectedItem = cmbAvatar.getSelectedItemPosition();
        int vIdFoto = arrAvatarResource.get(selectedItem);
        Person item = new Person(vNombre,vDNI,vEdad,vBiografia,vIdFoto,0);

        CustomerDBHelper db = new CustomerDBHelper(this);
        db.SavePerson(item);
        LimpiarFormulario();
        Toast.makeText(this, "Registro guardado satisfactoriamente!", Toast.LENGTH_SHORT).show();
    }

    private void LimpiarFormulario() {
        txtNombre.setText("");
        txtDNI.setText("");
        txtEdad.setText("");
        txtBiografia.setText("");
        cmbAvatar.setSelection(0);
    }
}
